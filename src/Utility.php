<?php

namespace Kernl;

class Utility
{
    /**
     * Retrieve Northeastern Brand Chrome
     * @param  string $url
     * @return string
     */
    public static function getBrandChrome($type)
    {
        // URL options
        $urls = [
            'css'    => 'https://www.northeastern.edu/nuglobalutils/common/css/headerfooter.css',
            'js'     => 'https://www.northeastern.edu/nuglobalutils/common/js/navigation-min.js',
            'header' => 'https://www.northeastern.edu/resources/components/?return=main-menu&cache=no',
            'footer' => 'https://www.northeastern.edu/resources/includes/?return=footer&cache=no'
        ];

        // Just return url for asset
        if ($type == 'css' || $type == 'js') {
            return $urls[$type];
        }

        // Get response for header/footer
        $args = [
            'method'    => 'GET',
            'timeout'   => 45,
            'sslverify' => false
        ];
        $response = wp_remote_request($urls[$type], $args);
        return wp_remote_retrieve_body($response);
    }

    /**
     * Retrieve Northeastern footer
     * @param  string $class
     * @return void
     */
    public static function getFooter($class = '')
    {
        return '
            <footer class="section footer '. $class .'" role="contentinfo">
                <a class="__logo" href="http://www.northeastern.edu" aria-label="Northeastern University logo">Northeastern University</a>
                <div class="__body">
                  <ul class="__list">
                    <li><a target="_blank" rel="noreferrer" href="https://my.northeastern.edu/welcome">myNortheastern</a></li>
                    <li><a target="_blank" rel="noreferrer" href="https://prod-web.neu.edu/webapp6/employeelookup/public/main.action">Find Faculty &amp; Staff</a></li>
                    <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/neuhome/adminlinks/findaz.html">Find A-Z</a></li>
                    <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/emergency/index.html">Emergency Information</a></li>
                    <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/search/index.html">Search</a></li>
                    <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/privacy/index.html">Privacy</a></li>
                  </ul>
                  <div class="__copy">
                    360 Huntington Ave., Boston, Massachusetts 02115 &nbsp;&bull;&nbsp; 617.373.2000 &nbsp;&bull;&nbsp; TTY 617.373.3768<br>
                    &copy; '. date('Y') .' Northeastern University
                  </div>
                </div>
            </footer>
        ';
    }

    /**
     * Retrieve Northeastern tag manager roll-up snippet
     * @param  string $environment
     * @return void
     */
    public static function getTagManager($type, $environment = 'development')
    {
        if ($environment === 'production') {
            if ($type == 'script') {
                return "
                    <!-- Google Tag Manager -->
                    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-WGQLLJ');</script>
                    <!-- End Google Tag Manager -->
                ";
            }

            if ($type == 'noscript') {
                return '
                    <!-- Google Tag Manager (noscript) -->
                    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WGQLLJ"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                    <!-- End Google Tag Manager (noscript) -->
                ';
            }
        }

        return '<!-- development environment: GTM -->';
    }

    /**
     * Get Google Analytics
     * @param  string $environment
     * @param  string $tracker
     * @return void
     */
    public static function getGoogleAnalytics($environment = 'development', $tracker)
    {
        if ($environment === 'production') {
            return "
                <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                  ga('create', '{$tracker}', 'auto');
                  ga('set', 'anonymizeIp', true);
                  ga('send', 'pageview');
                </script>
            ";
        } else {
            return "<!-- $environment ENV: GA {$tracker} -->";
        }
    }

    /**
     * Loads the a file within views/templates
     * @param  string $file path to file in views/templates
     * @return ob_get_contents
     */
    public static function getTemplate($path, $data)
    {
        // Sage function to locate compiled template
        $location = locate_template('views/templates/'. $path .'.blade.php');

        ob_start();
        extract($data);
        include \App\template_path($location);
        $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }

    /**
     * Remove empty paragraph tags
     * @param  string   $content
     * @return string
     */
    public static function removeEmptyParagraphs($content)
    {
        $content = force_balance_tags($content);
        $content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
        $content = preg_replace('~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content);
        return $content;
    }

    /**
     * Get full Tribe Events date
     * @param  string   $content
     * @return string
     */
    public static function getTribeDate($id, $format = 'M j, Y | g:i A')
    {
        // Remove time format if event is all day
        if (tribe_event_is_all_day()) {
            $format = 'M j, Y';
        }

        // Get start and end dates
        $start_date = tribe_get_start_date($id, false, $format);
        $end_date = tribe_get_end_date($id, false, $format);

        // Return multiday format
        if (tribe_event_is_multiday()) {
            return $start_date .' &mdash; '. $end_date;
        }

        // Return single day format
        return $start_date;
    }
}
